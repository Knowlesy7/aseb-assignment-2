﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZedGraph;

namespace assignment
{
    public partial class Graph : Form
    {
        public Graph()
        {
            InitializeComponent();
        }

        public double[] gheart;
        public double[] gpower;
        public double[] gspeed;
        public double[] gcadence;
        public double[] galtitude;
        public double[ ] powBal;
        public double[] powLeft, powRight, pIndex;
        double avgHR, avgPower, avgSpeed, avgCadence, avgAltitude, avgLeft, avgRight, avgPed;
        public DateTime[] gtime; 
        public int ya;
        public int yb;
        public int yc;
        public int yd;
        public int ye;
        public int x1;
        LineItem myCurve;
        LineItem myCurve2;
        LineItem myCurve3;
        LineItem myCurve4;
        LineItem myCurve5;

        GraphPane myPane, myPane2;
        PointPairList list1 = new PointPairList();
        PointPairList list2 = new PointPairList();
        PointPairList list3 = new PointPairList();
        PointPairList list4 = new PointPairList();
        PointPairList list5 = new PointPairList();
        PointPairList listLeft = new PointPairList();
        PointPairList listRight = new PointPairList();
        PointPairList listPedI = new PointPairList();
        FilteredPointList newHrate, newPower, newSpeed, newCadence, newAltitude, newLeft, newRight, newPedIn;



        public List<String> lines = new List<string>();

        private void Graph_Load(object sender, EventArgs e)
        {
            // Setup the graph
            CreateGraph(zedGraphControl1);
            CreatePieChart(zedGraphControl2);
            // Size the control to fill the form with a margin
            //SetSize();
        }

        private void SetSize()
        {
            zedGraphControl1.Location = new Point(10, 10);
            // Leave a small margin around the outside of the control
            zedGraphControl1.Size = new Size(ClientRectangle.Width - 20,
                                    ClientRectangle.Height - 20);
        }

        // Build the Chart
        private void CreateGraph(ZedGraphControl zgc)
        {
            // get a reference to the GraphPane
            myPane = zgc.GraphPane;

            myPane.CurveList.Clear();
            myPane.GraphObjList.Clear();
            myPane.YAxisList.Clear();

            //zgc.ZoomOutAll(myPane);

            ya = myPane.AddYAxis("Heart Rate(BPM)");
            yb = myPane.AddYAxis("Power(Watts)");
            yc = myPane.AddYAxis("Speed");
            yd = myPane.AddYAxis("Cadence");
            ye = myPane.AddYAxis("Altitude");

            // Set the Titles
            myPane.Title.Text = "Results for " + gtime[2].Day + "/" + gtime[1].Month + "/" +  gtime[0].Year + " in Graph Presentation";
            myPane.XAxis.Title.Text = "Time (HH:MM:SS)";
            myPane.XAxis.Type = AxisType.Date;
            myPane.XAxis.MajorGrid.IsVisible = true;
            myPane.YAxis.MajorGrid.IsVisible = true;
            myPane.XAxis.Scale.MajorUnit = DateUnit.Minute;
            myPane.XAxis.Scale.MinorUnit = DateUnit.Minute;
            myPane.XAxis.Scale.MajorStep = 5;
            myPane.XAxis.Scale.MinorStep = 5;
            myPane.XAxis.Scale.Format = "HH:mm:ss";

            // Make up some data arrays based on the Sine function
            double y1, y2, y3, y4, y5;
            double yLeft, yRight, yPed;
            XDate x;
            y1 = 0;
            y2 = 0;
            y3 = 0;
            y4 = 0;
            y5 = 0;

     

            for (int i = 0; i < 3979; i++)
            {
                x = new XDate(gtime[i].Year, gtime[i].Month, gtime[i].Day, gtime[i].Hour, gtime[i].Minute, gtime[i].Second);
                y1 = gheart[i];
                y2 = gpower[i];
                y3 = gspeed[i];
                y4 = gcadence[i];
                y5 = galtitude[i];
                yLeft = powLeft[i];
	            yRight = powRight[i];
	            yPed = pIndex[i];

                list1.Add(x, y1);
                list2.Add(x, y2);
                list3.Add(x, y3);
                list4.Add(x, y4);
                list5.Add(x, y5);
                listLeft.Add(x, yLeft);
                listRight.Add(x, yRight);
                listPedI.Add(x, yPed);
            }

            lines.Add("Heart Rate");
            lines.Add("Power");
            lines.Add("Speed");
            lines.Add("Cadence");
            lines.Add("Altitude");

            // Generate a red curve with diamond
            // symbols, and "Porsche" in the legend
            if (lines.Contains("Heart Rate"))
            {
                myCurve = new LineItem("Heart Rate (BPM)", list1, Color.Red, SymbolType.None) { YAxisIndex = ya };
                myPane.CurveList.Add(myCurve);
            }
            // Generate a blue curve with circle
            // symbols, and "Piper" in the legend
            if (lines.Contains("Power"))
            {
                myCurve2 = new LineItem("Power", list2, Color.Blue, SymbolType.None) { YAxisIndex = yb };
                myPane.CurveList.Add(myCurve2);
            }

            if (lines.Contains("Speed"))
            {
                myCurve3 = new LineItem("Speed", list3, Color.Green, SymbolType.None) { YAxisIndex = yc };
                myPane.CurveList.Add(myCurve3);
            }

            if (lines.Contains("Cadence"))
            {
                myCurve4 = new LineItem("Cadence", list4, Color.Purple, SymbolType.None) { YAxisIndex = yd };
                myPane.CurveList.Add(myCurve4);
            }

            if (lines.Contains("Altitude"))
            {
                myCurve5 = new LineItem("Altitude", list5, Color.Black, SymbolType.None) { YAxisIndex = ye };
                myPane.CurveList.Add(myCurve5);
            }

            myPane.Chart.Border.IsVisible = false;
            //For Heart Rate Axis
            myPane.YAxisList[ya].Title.IsVisible = false;
            myPane.YAxisList[ya].Scale.MinAuto = true;
            myPane.YAxisList[ya].Scale.MaxAuto = true;
            myPane.YAxisList[ya].Color = Color.Red;
            myPane.YAxisList[ya].Scale.FontSpec.FontColor = Color.Red;
            myPane.YAxisList[ya].MajorTic.Color = Color.Red;
            myPane.YAxisList[ya].MinorTic.Color = Color.Red;
            myPane.YAxisList[ya].MajorGrid.Color = Color.Red;
            myPane.YAxisList[ya].MinorGrid.Color = Color.Red;
            //For Power Axis
            myPane.YAxisList[yb].Title.IsVisible = false;
            myPane.YAxisList[yb].Scale.MinAuto = true;
            myPane.YAxisList[yb].Scale.MaxAuto = true;
            myPane.YAxisList[yb].Color = Color.Blue;
            myPane.YAxisList[yb].Scale.FontSpec.FontColor = Color.Blue;
            myPane.YAxisList[yb].MajorTic.Color = Color.Blue;
            myPane.YAxisList[yb].MinorTic.Color = Color.Blue;
            myPane.YAxisList[yb].MajorGrid.Color = Color.Blue;
            myPane.YAxisList[yb].MinorGrid.Color = Color.Blue;
            //For Speed Axis
            myPane.YAxisList[yc].Title.IsVisible = false;
            myPane.YAxisList[yc].Scale.MinAuto = true;
            myPane.YAxisList[yc].Scale.MaxAuto = true;
            myPane.YAxisList[yc].Color = Color.Green;
            myPane.YAxisList[yc].Scale.FontSpec.FontColor = Color.Green;
            myPane.YAxisList[yc].MajorTic.Color = Color.Green;
            myPane.YAxisList[yc].MinorTic.Color = Color.Green;
            myPane.YAxisList[yc].MajorGrid.Color = Color.Green;
            myPane.YAxisList[yc].MinorGrid.Color = Color.Green;
            //For Cadence Axis
            myPane.YAxisList[yd].Title.IsVisible = false;
            myPane.YAxisList[yd].Scale.MinAuto = true;
            myPane.YAxisList[yd].Scale.MaxAuto = true;
            myPane.YAxisList[yd].Color = Color.Purple;
            myPane.YAxisList[yd].Scale.FontSpec.FontColor = Color.Purple;
            myPane.YAxisList[yd].MajorTic.Color = Color.Purple;
            myPane.YAxisList[yd].MinorTic.Color = Color.Purple;
            myPane.YAxisList[yd].MajorGrid.Color = Color.Purple;
            myPane.YAxisList[yd].MinorGrid.Color = Color.Purple;


            //finding averages of data. 
            double[] heartarray = list1.Select(p => p.Y).ToArray();
            double[] powerarray = list2.Select(p => p.Y).ToArray();
            double[] speedarray = list3.Select(p => p.Y).ToArray();
            double[] cadencearray = list4.Select(p => p.Y).ToArray();
            double[] altarray = list5.Select(p => p.Y).ToArray();
            double[] leftarray = listLeft.Select(p => p.Y).ToArray();
            double[] rightarray = listRight.Select(p => p.Y).ToArray();
            double[] pedindarray = listPedI.Select(p => p.Y).ToArray();
            double[] xvalue = list1.Select(p => p.X).ToArray();

            newHrate = new FilteredPointList(xvalue, heartarray);
            newPower = new FilteredPointList(xvalue, powerarray);
            newSpeed = new FilteredPointList(xvalue, speedarray);
            newCadence = new FilteredPointList(xvalue, cadencearray);
            newAltitude = new FilteredPointList(xvalue, altarray);
            newLeft = new FilteredPointList(xvalue,leftarray);
            newRight = new FilteredPointList(xvalue, rightarray);
            newPedIn = new FilteredPointList(xvalue, pedindarray);
            
            //finding values of averages and putting them as labels
            avgHR = heartarray.Average();
            avgHR = Math.Round(avgHR, 2); 
            avgPower = powerarray.Average();
            avgPower = Math.Round(avgPower, 2);
            avgSpeed = speedarray.Average();
            avgSpeed = Math.Round(avgSpeed, 2);
            avgCadence = cadencearray.Average();
            avgCadence = Math.Round(avgCadence, 2);
            avgAltitude = altarray.Average();
            avgAltitude = Math.Round(avgAltitude, 2);
            avgLeft = leftarray.Average();
            avgLeft = Math.Round(avgLeft, 2);
            avgRight = 100 - avgLeft;
            //avgRight = rightarray.Average();
            //avgRight = Math.Round(avgLeft, 2);
            avgPed = pedindarray.Average();
            avgPed = Math.Round(avgLeft, 2);

            label1.Text = "Average Heart Rate: " + avgHR.ToString();
            label2.Text = "Average Power: " + avgPower.ToString();
            label3.Text = "Average Speed: " + avgSpeed.ToString();
            label4.Text = "Average Cadence: " + avgCadence.ToString();
            label5.Text = "Average Altitude: " + avgAltitude.ToString();
    

            // Tell ZedGraph to refigure the
            // axes since the data have changed
            zgc.Invalidate();
            zgc.Refresh();
            zgc.AxisChange();
        }

        private void zedGraphControl1_ZoomEvent(ZedGraphControl sender, ZoomState oldState, ZoomState newState)
        {
            // The maximum number of point to displayed is based on the width of the graphpane, and the visible range of the X axis
            newHrate.SetBounds(sender.GraphPane.XAxis.Scale.Min, sender.GraphPane.XAxis.Scale.Max, (int)zedGraphControl1.GraphPane.Rect.Width);
            newPower.SetBounds(sender.GraphPane.XAxis.Scale.Min, sender.GraphPane.XAxis.Scale.Max, (int)zedGraphControl1.GraphPane.Rect.Width);
            newSpeed.SetBounds(sender.GraphPane.XAxis.Scale.Min, sender.GraphPane.XAxis.Scale.Max, (int)zedGraphControl1.GraphPane.Rect.Width);
            newCadence.SetBounds(sender.GraphPane.XAxis.Scale.Min, sender.GraphPane.XAxis.Scale.Max, (int)zedGraphControl1.GraphPane.Rect.Width);
            newAltitude.SetBounds(sender.GraphPane.XAxis.Scale.Min, sender.GraphPane.XAxis.Scale.Max, (int)zedGraphControl1.GraphPane.Rect.Width);
            newLeft.SetBounds(sender.GraphPane.XAxis.Scale.Min, sender.GraphPane.XAxis.Scale.Max, (int)zedGraphControl1.GraphPane.Rect.Width);
            newRight.SetBounds(sender.GraphPane.XAxis.Scale.Min, sender.GraphPane.XAxis.Scale.Max, (int)zedGraphControl1.GraphPane.Rect.Width);
            newPedIn.SetBounds(sender.GraphPane.XAxis.Scale.Min, sender.GraphPane.XAxis.Scale.Max, (int)zedGraphControl1.GraphPane.Rect.Width);

            // This refreshes the graph when the button is released after a panning operation
            if (newState.Type == ZoomState.StateType.Zoom)
            {
                int zoomCount = Convert.ToInt32(newHrate.Count.ToString());
                double hrTotal, pTotal, sTotal, cTotal, aTotal, lTotal, rTotal, piTotal;
                hrTotal = pTotal = sTotal = cTotal = aTotal = lTotal = rTotal = piTotal = 0;
                for (int i = 0; i < zoomCount; i++)
                {
                    var zoom1 = newHrate[i].ToString();
                    //MessageBox.Show(newHrate[i].ToString());
                    var textHR = zoom1.Split(',');
                    textHR[1] = textHR[1].Trim();
                    var hrTrim = textHR[1].Split(' ');
                    hrTotal += Convert.ToDouble(hrTrim[0]);

                    var zoom2 = newPower[i].ToString();
                    //MessageBox.Show(newHrate[i].ToString());
                    var textP = zoom2.Split(',');
                    textP[1] = textP[1].Trim();
                    var PTrim = textP[1].Split(' ');
                    pTotal += Convert.ToDouble(PTrim[0]);

                    var zoom3 = newSpeed[i].ToString();
                    //MessageBox.Show(newHrate[i].ToString());
                    var textS = zoom3.Split(',');
                    textS[1] = textS[1].Trim();
                    var STrim = textS[1].Split(' ');
                    sTotal += Convert.ToDouble(STrim[0]);

                    var zoom4 = newCadence[i].ToString();
                    //MessageBox.Show(newHrate[i].ToString());
                    var textC = zoom4.Split(',');
                    textC[1] = textC[1].Trim();
                    var CTrim = textC[1].Split(' ');
                    cTotal += Convert.ToDouble(CTrim[0]);

                    var zoom5 = newAltitude[i].ToString();
                    //MessageBox.Show(newHrate[i].ToString());
                    var textA = zoom5.Split(',');
                    textA[1] = textA[1].Trim();
                    var ATrim = textA[1].Split(' ');
                    aTotal += Convert.ToDouble(ATrim[0]);

                    var zoom6 = newLeft[i].ToString();
                    //MessageBox.Show(newHrate[i].ToString());
                    var textL = zoom6.Split(',');
                    textL[1] = textL[1].Trim();
                    var LTrim = textL[1].Split(' ');
                    lTotal += Convert.ToDouble(LTrim[0]);

                    var zoom7 = newCadence[i].ToString();
                    //MessageBox.Show(newHrate[i].ToString());
                    var textR = zoom7.Split(',');
                    textR[1] = textR[1].Trim();
                    var RTrim = textR[1].Split(' ');
                    rTotal += Convert.ToDouble(RTrim[0]);

                    var zoom8 = newCadence[i].ToString();
                    //MessageBox.Show(newHrate[i].ToString());
                    var textPI = zoom8.Split(',');
                    textPI[1] = textC[1].Trim();
                    var PITrim = textPI[1].Split(' ');
                    piTotal += Convert.ToDouble(PITrim[0]);




                    //Work out your averages in here
                    //Get total from newHrate[i]
                    //Divide by zoom count
                    
                }
                avgHR = hrTotal/zoomCount;
                avgHR = Math.Round(avgHR, 2);
                avgPower = pTotal / zoomCount;
                avgPower = Math.Round(avgPower, 2);
                avgSpeed = sTotal/zoomCount;
                avgSpeed = Math.Round(avgSpeed, 2);
                avgCadence = cTotal/ zoomCount;
                avgCadence = Math.Round(avgCadence, 2);
                avgAltitude = aTotal/zoomCount;
                avgAltitude = Math.Round(avgAltitude, 2);
                avgLeft = lTotal / zoomCount;
                avgLeft = Math.Round(avgLeft, 2);
                avgRight = 100 - avgLeft;
                //avgRight = rTotal/zoomCount;
                //avgRight = Math.Round(avgLeft, 2);
                avgPed = piTotal/zoomCount;
                avgPed = Math.Round(avgLeft, 2);

                label1.Text = "Average Heart Rate: " + avgHR.ToString();
                label2.Text = "Average Power: " + avgPower.ToString();
                label3.Text = "Average Speed: " + avgSpeed.ToString();
                label4.Text = "Average Cadence: " + avgCadence.ToString();
                label5.Text = "Average Altitude: " + avgAltitude.ToString();
                sender.Invalidate();

            myPane2.CurveList.Clear();

            myPane2.Title.Text = "Power Balance";
            myPane2.Title.FontSpec.Size = 30;
            myPane2.Legend.FontSpec.Size = 25;  

            myPane2.CurveList.Clear();
            //MessageBox.Show(avgLeft.ToString());
            //MessageBox.Show(avgRight.ToString());
            PieItem item1 = myPane2.AddPieSlice(avgLeft, Color.Red, 0, "Left");
            PieItem item2 = myPane2.AddPieSlice(avgRight, Color.Green, 0, "Right");

            item1.LabelType = PieLabelType.Name_Percent;
            item1.LabelDetail.FontSpec.Size = 20;
            item2.LabelType = PieLabelType.Name_Percent;
            item2.LabelDetail.FontSpec.Size = 20;

            // Tell ZedGraph to refigure the
            // axes since the data have changed
            zedGraphControl2.AxisChange();
            zedGraphControl2.Invalidate();
            zedGraphControl2.Refresh();
            }
        }

        private void zedGraphControl1_ScrollProgressEvent(ZedGraphControl sender, ScrollBar scrollBar, ZoomState oldState, ZoomState newState)
        {
            // The maximum number of point to displayed is based on the width of the graphpane, and the visible range of the X axis
            newHrate.SetBounds(sender.GraphPane.XAxis.Scale.Min, sender.GraphPane.XAxis.Scale.Max, (int)zedGraphControl1.GraphPane.Rect.Width);
            newPower.SetBounds(sender.GraphPane.XAxis.Scale.Min, sender.GraphPane.XAxis.Scale.Max, (int)zedGraphControl1.GraphPane.Rect.Width);
            newSpeed.SetBounds(sender.GraphPane.XAxis.Scale.Min, sender.GraphPane.XAxis.Scale.Max, (int)zedGraphControl1.GraphPane.Rect.Width);
            newCadence.SetBounds(sender.GraphPane.XAxis.Scale.Min, sender.GraphPane.XAxis.Scale.Max, (int)zedGraphControl1.GraphPane.Rect.Width);
            newAltitude.SetBounds(sender.GraphPane.XAxis.Scale.Min, sender.GraphPane.XAxis.Scale.Max, (int)zedGraphControl1.GraphPane.Rect.Width);
            newLeft.SetBounds(sender.GraphPane.XAxis.Scale.Min, sender.GraphPane.XAxis.Scale.Max, (int)zedGraphControl1.GraphPane.Rect.Width);
            newRight.SetBounds(sender.GraphPane.XAxis.Scale.Min, sender.GraphPane.XAxis.Scale.Max, (int)zedGraphControl1.GraphPane.Rect.Width);
            newPedIn.SetBounds(sender.GraphPane.XAxis.Scale.Min, sender.GraphPane.XAxis.Scale.Max, (int)zedGraphControl1.GraphPane.Rect.Width);

            // This refreshes the graph when the button is released after a panning operation
            if (newState.Type == ZoomState.StateType.Zoom)
            {
                int zoomCount = Convert.ToInt32(newHrate.Count.ToString());
                double hrTotal, pTotal, sTotal, cTotal, aTotal, lTotal, rTotal, piTotal;
                hrTotal = pTotal = sTotal = cTotal = aTotal = lTotal = rTotal = piTotal = 0;
                for (int i = 0; i < zoomCount; i++)
                {
                    var zoom1 = newHrate[i].ToString();
                    //MessageBox.Show(newHrate[i].ToString());
                    var textHR = zoom1.Split(',');
                    textHR[1] = textHR[1].Trim();
                    var hrTrim = textHR[1].Split(' ');
                    hrTotal += Convert.ToDouble(hrTrim[0]);

                    var zoom2 = newPower[i].ToString();
                    //MessageBox.Show(newHrate[i].ToString());
                    var textP = zoom2.Split(',');
                    textP[1] = textP[1].Trim();
                    var PTrim = textP[1].Split(' ');
                    pTotal += Convert.ToDouble(PTrim[0]);

                    var zoom3 = newSpeed[i].ToString();
                    //MessageBox.Show(newHrate[i].ToString());
                    var textS = zoom3.Split(',');
                    textS[1] = textS[1].Trim();
                    var STrim = textS[1].Split(' ');
                    sTotal += Convert.ToDouble(STrim[0]);

                    var zoom4 = newCadence[i].ToString();
                    //MessageBox.Show(newHrate[i].ToString());
                    var textC = zoom4.Split(',');
                    textC[1] = textC[1].Trim();
                    var CTrim = textC[1].Split(' ');
                    cTotal += Convert.ToDouble(CTrim[0]);

                    var zoom5 = newAltitude[i].ToString();
                    //MessageBox.Show(newHrate[i].ToString());
                    var textA = zoom5.Split(',');
                    textA[1] = textA[1].Trim();
                    var ATrim = textA[1].Split(' ');
                    aTotal += Convert.ToDouble(ATrim[0]);

                    var zoom6 = newLeft[i].ToString();
                    //MessageBox.Show(newHrate[i].ToString());
                    var textL = zoom6.Split(',');
                    textL[1] = textL[1].Trim();
                    var LTrim = textL[1].Split(' ');
                    lTotal += Convert.ToDouble(LTrim[0]);

                    var zoom7 = newCadence[i].ToString();
                    //MessageBox.Show(newHrate[i].ToString());
                    var textR = zoom7.Split(',');
                    textR[1] = textR[1].Trim();
                    var RTrim = textR[1].Split(' ');
                    rTotal += Convert.ToDouble(RTrim[0]);

                    var zoom8 = newCadence[i].ToString();
                    //MessageBox.Show(newHrate[i].ToString());
                    var textPI = zoom8.Split(',');
                    textPI[1] = textC[1].Trim();
                    var PITrim = textPI[1].Split(' ');
                    piTotal += Convert.ToDouble(PITrim[0]);


                }
                avgHR = hrTotal / zoomCount;
                avgHR = Math.Round(avgHR, 2);
                avgPower = pTotal / zoomCount;
                avgPower = Math.Round(avgPower, 2);
                avgSpeed = sTotal / zoomCount;
                avgSpeed = Math.Round(avgSpeed, 2);
                avgCadence = cTotal / zoomCount;
                avgCadence = Math.Round(avgCadence, 2);
                avgAltitude = aTotal / zoomCount;
                avgAltitude = Math.Round(avgAltitude, 2);
                avgLeft = lTotal / zoomCount;
                avgLeft = Math.Round(avgLeft, 2);
                avgRight = 100 - avgLeft;
                //avgRight = rTotal / zoomCount;
                //avgRight = Math.Round(avgLeft, 2);
                avgPed = piTotal / zoomCount;
                avgPed = Math.Round(avgLeft, 2);

                label1.Text = "Average Heart Rate: " + avgHR.ToString();
                label2.Text = "Average Power: " + avgPower.ToString();
                label3.Text = "Average Speed: " + avgSpeed.ToString();
                label4.Text = "Average Cadence: " + avgCadence.ToString();
                label5.Text = "Average Altitude: " + avgAltitude.ToString();
                sender.Invalidate();

                myPane2.CurveList.Clear();

                myPane2.Title.Text = "Power Balance";
                myPane2.Title.FontSpec.Size = 30;
                myPane2.Legend.FontSpec.Size = 25;

                myPane2.CurveList.Clear();

                PieItem item1 = myPane2.AddPieSlice(avgLeft, Color.Red, 0, "Left");
                PieItem item2 = myPane2.AddPieSlice(avgRight, Color.Green, 0, "Right");

                item1.LabelType = PieLabelType.Name_Percent;
                item1.LabelDetail.FontSpec.Size = 20;
                item2.LabelType = PieLabelType.Name_Percent;
                item2.LabelDetail.FontSpec.Size = 20;

                // Tell ZedGraph to refigure the
                // axes since the data have changed
                zedGraphControl2.AxisChange();
                zedGraphControl2.Invalidate();
                zedGraphControl2.Refresh();
            }

        }

        private void Graph_Resize(object sender, EventArgs e)
        {
            //SetSize();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            ZedGraph.CurveItem removeCurve = myCurve;

            if (checkBox1.Checked)
            {
                zedGraphControl1.GraphPane.CurveList.Remove(removeCurve);
            }
            else
            {
                zedGraphControl1.GraphPane.CurveList.Add(removeCurve);
            }

            zedGraphControl1.Refresh();
        }

        private void checkBox4_CheckedChanged(object sender, EventArgs e)
        {
            ZedGraph.CurveItem removeCurve = myCurve2;

            if (checkBox4.Checked)
            {
                zedGraphControl1.GraphPane.CurveList.Remove(removeCurve);
            }
            else
            {
                zedGraphControl1.GraphPane.CurveList.Add(removeCurve);
            }

            zedGraphControl1.Refresh();
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            ZedGraph.CurveItem removeCurve = myCurve3;

            if (checkBox2.Checked)
            {
                zedGraphControl1.GraphPane.CurveList.Remove(removeCurve);
            }
            else
            {
                zedGraphControl1.GraphPane.CurveList.Add(removeCurve);
            }

            zedGraphControl1.Refresh();
        }


        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {
            ZedGraph.CurveItem removeCurve = myCurve4;

            if (checkBox3.Checked)
            {
                zedGraphControl1.GraphPane.CurveList.Remove(removeCurve);
            }
            else
            {
                zedGraphControl1.GraphPane.CurveList.Add(removeCurve);
            }

            zedGraphControl1.Refresh();
        }

        private void checkBox5_CheckedChanged(object sender, EventArgs e)
        {
            ZedGraph.CurveItem removeCurve = myCurve5;

            if (checkBox5.Checked)
            {
                zedGraphControl1.GraphPane.CurveList.Remove(removeCurve);
            }
            else
            {
                zedGraphControl1.GraphPane.CurveList.Add(removeCurve);
            }

            zedGraphControl1.Refresh();
        }

        private void CreatePieChart(ZedGraphControl zgc2)
        {
            // get a reference to the GraphPane
            myPane2 = zgc2.GraphPane;

            myPane2.Title.Text = "Power Balance";
            myPane2.Title.FontSpec.Size = 30;
            myPane2.Legend.FontSpec.Size = 25;  

            myPane2.CurveList.Clear();

            PieItem item1 = myPane2.AddPieSlice(avgLeft, Color.Red, 0, "Left");
            PieItem item2 = myPane2.AddPieSlice(avgRight, Color.Green, 0, "Right");

            item1.LabelType = PieLabelType.Name_Percent;
            item1.LabelDetail.FontSpec.Size = 20;
            item2.LabelType = PieLabelType.Name_Percent;
            item2.LabelDetail.FontSpec.Size = 20;

            // Tell ZedGraph to refigure the
            // axes since the data have changed
            zgc2.AxisChange();
            zgc2.Invalidate();
            zgc2.Refresh();
        }







    }
}

