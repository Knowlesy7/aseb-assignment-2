﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZedGraph;

namespace assignment
{
    public partial class PieChart : Form
    {
        public PieChart()
        {
            InitializeComponent();
        }


        private void SetSize()
        {
            zedGraphControl3.Location = new Point(10, 10);
            // Leave a small margin around the outside of the control
            zedGraphControl3.Size = new Size(ClientRectangle.Width - 20,
                                    ClientRectangle.Height - 20);
        }

        public void CreateChart(ZedGraphControl zgc)
        {
            GraphPane myPane1 = zgc.GraphPane;

            // Set the GraphPane title
            myPane1.Title.Text = "2004 ZedGraph Sales by Region\n($M)";
            myPane1.Title.FontSpec.IsItalic = true;
            myPane1.Title.FontSpec.Size = 24f;
            myPane1.Title.FontSpec.Family = "Times New Roman";

            // Fill the pane background with a color gradient
            myPane1.Fill = new Fill(Color.White, Color.Goldenrod, 45.0f);
            // No fill for the chart background
            myPane1.Chart.Fill.Type = FillType.None;

            // Set the legend to an arbitrary location
            myPane1.Legend.Position = LegendPos.Float;
            myPane1.Legend.Location = new Location(0.95f, 0.15f, CoordType.PaneFraction,
                           AlignH.Right, AlignV.Top);
            myPane1.Legend.FontSpec.Size = 10f;
            myPane1.Legend.IsHStack = false;

            // Add some pie slices
            PieItem segment1 = myPane1.AddPieSlice(20, Color.Navy, Color.White, 45f, 0, "North");
            PieItem segment3 = myPane1.AddPieSlice(30, Color.Purple, Color.White, 45f, .0, "East");
            PieItem segment4 = myPane1.AddPieSlice(10.21, Color.LimeGreen, Color.White, 45f, 0, "West");
            PieItem segment2 = myPane1.AddPieSlice(40, Color.SandyBrown, Color.White, 45f, 0.2, "South");
            PieItem segment6 = myPane1.AddPieSlice(250, Color.Red, Color.White, 45f, 0, "Europe");
            PieItem segment7 = myPane1.AddPieSlice(1500, Color.Blue, Color.White, 45f, 0.2, "Pac Rim");
            PieItem segment8 = myPane1.AddPieSlice(400, Color.Green, Color.White, 45f, 0, "South America");
            PieItem segment9 = myPane1.AddPieSlice(50, Color.Yellow, Color.White, 45f, 0.2, "Africa");

            segment2.LabelDetail.FontSpec.FontColor = Color.Red;

            // Sum up the pie values                                                               
            CurveList curves = myPane1.CurveList;
            double total = 0;
            for (int x = 0; x < curves.Count; x++)
                total += ((PieItem)curves[x]).Value;

            // Make a text label to highlight the total value
            TextObj text = new TextObj("Total 2004 Sales\n" + "$" + total.ToString() + "M",
                           0.18F, 0.40F, CoordType.PaneFraction);
            text.Location.AlignH = AlignH.Center;
            text.Location.AlignV = AlignV.Bottom;
            text.FontSpec.Border.IsVisible = false;
            text.FontSpec.Fill = new Fill(Color.White, Color.FromArgb(255, 100, 100), 45F);
            text.FontSpec.StringAlignment = StringAlignment.Center;
            myPane1.GraphObjList.Add(text);

            // Create a drop shadow for the total value text item
            TextObj text2 = new TextObj(text);
            text2.FontSpec.Fill = new Fill(Color.Black);
            text2.Location.X += 0.008f;
            text2.Location.Y += 0.01f;
            myPane1.GraphObjList.Add(text2);

            // Calculate the Axis Scale Ranges
            zgc.AxisChange();
        }
        private void Graph_Load(object sender, EventArgs e)
        {
            // Setup the graph
            CreateChart(zedGraphControl3);
            // Size the control to fill the form with a margin
            //SetSize();
        }

    }
}

