﻿namespace assignment
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.header = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.time = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.heartrate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.speed = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cadence = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.altitude = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.power = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.powerBalance = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.totalDistance = new System.Windows.Forms.Label();
            this.averageSpeed = new System.Windows.Forms.Label();
            this.maxSpeed = new System.Windows.Forms.Label();
            this.averagePower = new System.Windows.Forms.Label();
            this.averageHR = new System.Windows.Forms.Label();
            this.maxHR = new System.Windows.Forms.Label();
            this.minHR = new System.Windows.Forms.Label();
            this.maxPower = new System.Windows.Forms.Label();
            this.averageAltitude = new System.Windows.Forms.Label();
            this.maxAltitude = new System.Windows.Forms.Label();
            this.version = new System.Windows.Forms.Label();
            this.monitor = new System.Windows.Forms.Label();
            this.smode = new System.Windows.Forms.Label();
            this.date = new System.Windows.Forms.Label();
            this.startTime = new System.Windows.Forms.Label();
            this.maxHeart = new System.Windows.Forms.Label();
            this.restHR = new System.Windows.Forms.Label();
            this.duration = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.PBLeft = new System.Windows.Forms.Label();
            this.PBRight = new System.Windows.Forms.Label();
            this.PedInd = new System.Windows.Forms.Label();
            this.button5 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(764, 246);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(70, 69);
            this.listBox1.TabIndex = 0;
            // 
            // header
            // 
            this.header.Location = new System.Drawing.Point(10, 49);
            this.header.Name = "header";
            this.header.Size = new System.Drawing.Size(75, 23);
            this.header.TabIndex = 3;
            this.header.Text = "Upload";
            this.header.UseVisualStyleBackColor = true;
            this.header.Click += new System.EventHandler(this.header_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.time,
            this.heartrate,
            this.speed,
            this.cadence,
            this.altitude,
            this.power,
            this.powerBalance});
            this.dataGridView1.Location = new System.Drawing.Point(99, 28);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(748, 282);
            this.dataGridView1.TabIndex = 4;
            // 
            // time
            // 
            this.time.HeaderText = "Time";
            this.time.Name = "time";
            this.time.ReadOnly = true;
            // 
            // heartrate
            // 
            this.heartrate.HeaderText = "Heart Rate";
            this.heartrate.Name = "heartrate";
            // 
            // speed
            // 
            this.speed.HeaderText = "Speed";
            this.speed.Name = "speed";
            // 
            // cadence
            // 
            this.cadence.HeaderText = "Cadence";
            this.cadence.Name = "cadence";
            // 
            // altitude
            // 
            this.altitude.HeaderText = "Altitude";
            this.altitude.Name = "altitude";
            // 
            // power
            // 
            this.power.HeaderText = "Power";
            this.power.Name = "power";
            // 
            // powerBalance
            // 
            this.powerBalance.HeaderText = "Power Balance";
            this.powerBalance.Name = "powerBalance";
            // 
            // totalDistance
            // 
            this.totalDistance.AutoSize = true;
            this.totalDistance.Location = new System.Drawing.Point(383, 339);
            this.totalDistance.Name = "totalDistance";
            this.totalDistance.Size = new System.Drawing.Size(79, 13);
            this.totalDistance.TabIndex = 5;
            this.totalDistance.Text = "Total Distance:";
            // 
            // averageSpeed
            // 
            this.averageSpeed.AutoSize = true;
            this.averageSpeed.Location = new System.Drawing.Point(383, 365);
            this.averageSpeed.Name = "averageSpeed";
            this.averageSpeed.Size = new System.Drawing.Size(84, 13);
            this.averageSpeed.TabIndex = 6;
            this.averageSpeed.Text = "Average Speed:";
            // 
            // maxSpeed
            // 
            this.maxSpeed.AutoSize = true;
            this.maxSpeed.Location = new System.Drawing.Point(383, 391);
            this.maxSpeed.Name = "maxSpeed";
            this.maxSpeed.Size = new System.Drawing.Size(88, 13);
            this.maxSpeed.TabIndex = 7;
            this.maxSpeed.Text = "Maximum Speed:";
            // 
            // averagePower
            // 
            this.averagePower.AutoSize = true;
            this.averagePower.Location = new System.Drawing.Point(383, 417);
            this.averagePower.Name = "averagePower";
            this.averagePower.Size = new System.Drawing.Size(83, 13);
            this.averagePower.TabIndex = 8;
            this.averagePower.Text = "Average Power:";
            // 
            // averageHR
            // 
            this.averageHR.AutoSize = true;
            this.averageHR.Location = new System.Drawing.Point(599, 339);
            this.averageHR.Name = "averageHR";
            this.averageHR.Size = new System.Drawing.Size(105, 13);
            this.averageHR.TabIndex = 10;
            this.averageHR.Text = "Average Heart Rate:";
            // 
            // maxHR
            // 
            this.maxHR.AutoSize = true;
            this.maxHR.Location = new System.Drawing.Point(599, 365);
            this.maxHR.Name = "maxHR";
            this.maxHR.Size = new System.Drawing.Size(109, 13);
            this.maxHR.TabIndex = 11;
            this.maxHR.Text = "Maximum Heart Rate:";
            // 
            // minHR
            // 
            this.minHR.AutoSize = true;
            this.minHR.Location = new System.Drawing.Point(599, 391);
            this.minHR.Name = "minHR";
            this.minHR.Size = new System.Drawing.Size(106, 13);
            this.minHR.TabIndex = 12;
            this.minHR.Text = "Minimum Heart Rate:";
            // 
            // maxPower
            // 
            this.maxPower.AutoSize = true;
            this.maxPower.Location = new System.Drawing.Point(383, 442);
            this.maxPower.Name = "maxPower";
            this.maxPower.Size = new System.Drawing.Size(87, 13);
            this.maxPower.TabIndex = 13;
            this.maxPower.Text = "Maximum Power:";
            // 
            // averageAltitude
            // 
            this.averageAltitude.AutoSize = true;
            this.averageAltitude.Location = new System.Drawing.Point(599, 417);
            this.averageAltitude.Name = "averageAltitude";
            this.averageAltitude.Size = new System.Drawing.Size(88, 13);
            this.averageAltitude.TabIndex = 14;
            this.averageAltitude.Text = "Average Altitude:";
            // 
            // maxAltitude
            // 
            this.maxAltitude.AutoSize = true;
            this.maxAltitude.Location = new System.Drawing.Point(599, 442);
            this.maxAltitude.Name = "maxAltitude";
            this.maxAltitude.Size = new System.Drawing.Size(92, 13);
            this.maxAltitude.TabIndex = 15;
            this.maxAltitude.Text = "Maximum Altitude:";
            // 
            // version
            // 
            this.version.AutoSize = true;
            this.version.Location = new System.Drawing.Point(27, 338);
            this.version.Name = "version";
            this.version.Size = new System.Drawing.Size(45, 13);
            this.version.TabIndex = 16;
            this.version.Text = "Version:";
            // 
            // monitor
            // 
            this.monitor.AutoSize = true;
            this.monitor.Location = new System.Drawing.Point(27, 365);
            this.monitor.Name = "monitor";
            this.monitor.Size = new System.Drawing.Size(45, 13);
            this.monitor.TabIndex = 17;
            this.monitor.Text = "Monitor:";
            // 
            // smode
            // 
            this.smode.AutoSize = true;
            this.smode.Location = new System.Drawing.Point(27, 391);
            this.smode.Name = "smode";
            this.smode.Size = new System.Drawing.Size(47, 13);
            this.smode.TabIndex = 18;
            this.smode.Text = "S Mode:";
            // 
            // date
            // 
            this.date.AutoSize = true;
            this.date.Location = new System.Drawing.Point(27, 417);
            this.date.Name = "date";
            this.date.Size = new System.Drawing.Size(33, 13);
            this.date.TabIndex = 19;
            this.date.Text = "Date:";
            // 
            // startTime
            // 
            this.startTime.AutoSize = true;
            this.startTime.Location = new System.Drawing.Point(27, 442);
            this.startTime.Name = "startTime";
            this.startTime.Size = new System.Drawing.Size(58, 13);
            this.startTime.TabIndex = 20;
            this.startTime.Text = "Start Time:";
            // 
            // maxHeart
            // 
            this.maxHeart.AutoSize = true;
            this.maxHeart.Location = new System.Drawing.Point(27, 492);
            this.maxHeart.Name = "maxHeart";
            this.maxHeart.Size = new System.Drawing.Size(49, 13);
            this.maxHeart.TabIndex = 21;
            this.maxHeart.Text = "Max HR:";
            // 
            // restHR
            // 
            this.restHR.AutoSize = true;
            this.restHR.Location = new System.Drawing.Point(27, 517);
            this.restHR.Name = "restHR";
            this.restHR.Size = new System.Drawing.Size(51, 13);
            this.restHR.TabIndex = 22;
            this.restHR.Text = "Rest HR:";
            // 
            // duration
            // 
            this.duration.AutoSize = true;
            this.duration.Location = new System.Drawing.Point(27, 467);
            this.duration.Name = "duration";
            this.duration.Size = new System.Drawing.Size(50, 13);
            this.duration.TabIndex = 23;
            this.duration.Text = "Duration:";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(655, 577);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(89, 23);
            this.button1.TabIndex = 24;
            this.button1.Text = "Display as KM";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(750, 577);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(97, 23);
            this.button2.TabIndex = 25;
            this.button2.Text = "Display as miles";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 541);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 13);
            this.label1.TabIndex = 26;
            this.label1.Text = "Interval:";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(10, 94);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 27;
            this.button3.Text = "Graph";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(10, 134);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 28;
            this.button4.Text = "Calendar";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // PBLeft
            // 
            this.PBLeft.AutoSize = true;
            this.PBLeft.Location = new System.Drawing.Point(144, 338);
            this.PBLeft.Name = "PBLeft";
            this.PBLeft.Size = new System.Drawing.Size(100, 13);
            this.PBLeft.TabIndex = 29;
            this.PBLeft.Text = "Power Balance Left";
            // 
            // PBRight
            // 
            this.PBRight.AutoSize = true;
            this.PBRight.Location = new System.Drawing.Point(144, 364);
            this.PBRight.Name = "PBRight";
            this.PBRight.Size = new System.Drawing.Size(107, 13);
            this.PBRight.TabIndex = 30;
            this.PBRight.Text = "Power Balance Right";
            // 
            // PedInd
            // 
            this.PedInd.AutoSize = true;
            this.PedInd.Location = new System.Drawing.Point(147, 389);
            this.PedInd.Name = "PedInd";
            this.PedInd.Size = new System.Drawing.Size(63, 13);
            this.PedInd.TabIndex = 31;
            this.PedInd.Text = "Pedal Index";
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(10, 163);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 23);
            this.button5.TabIndex = 32;
            this.button5.Text = "Save";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(859, 612);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.PedInd);
            this.Controls.Add(this.PBRight);
            this.Controls.Add(this.PBLeft);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.duration);
            this.Controls.Add(this.restHR);
            this.Controls.Add(this.maxHeart);
            this.Controls.Add(this.startTime);
            this.Controls.Add(this.date);
            this.Controls.Add(this.smode);
            this.Controls.Add(this.monitor);
            this.Controls.Add(this.version);
            this.Controls.Add(this.maxAltitude);
            this.Controls.Add(this.averageAltitude);
            this.Controls.Add(this.maxPower);
            this.Controls.Add(this.minHR);
            this.Controls.Add(this.maxHR);
            this.Controls.Add(this.averageHR);
            this.Controls.Add(this.averagePower);
            this.Controls.Add(this.maxSpeed);
            this.Controls.Add(this.averageSpeed);
            this.Controls.Add(this.totalDistance);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.header);
            this.Controls.Add(this.listBox1);
            this.Name = "Form1";
            this.Text = "Cycle Data";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label totalDistance;
        private System.Windows.Forms.Label averageSpeed;
        private System.Windows.Forms.Label maxSpeed;
        private System.Windows.Forms.Label averagePower;
        private System.Windows.Forms.Label averageHR;
        private System.Windows.Forms.Label maxHR;
        private System.Windows.Forms.Label minHR;
        private System.Windows.Forms.Label maxPower;
        private System.Windows.Forms.Label averageAltitude;
        private System.Windows.Forms.Label maxAltitude;
        private System.Windows.Forms.DataGridViewTextBoxColumn time;
        private System.Windows.Forms.DataGridViewTextBoxColumn heartrate;
        private System.Windows.Forms.DataGridViewTextBoxColumn speed;
        private System.Windows.Forms.DataGridViewTextBoxColumn cadence;
        private System.Windows.Forms.DataGridViewTextBoxColumn altitude;
        private System.Windows.Forms.DataGridViewTextBoxColumn power;
        private System.Windows.Forms.DataGridViewTextBoxColumn powerBalance;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        public System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label PBLeft;
        private System.Windows.Forms.Label PBRight;
        private System.Windows.Forms.Label PedInd;
        private System.Windows.Forms.Button button5;
        public System.Windows.Forms.Label version;
        public System.Windows.Forms.Label monitor;
        public System.Windows.Forms.Label smode;
        public System.Windows.Forms.Label date;
        public System.Windows.Forms.Label startTime;
        public System.Windows.Forms.Label maxHeart;
        public System.Windows.Forms.Label restHR;
        public System.Windows.Forms.Label duration;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.ListBox listBox1;
        public System.Windows.Forms.Button header;
    }
}

