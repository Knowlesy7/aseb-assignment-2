﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZedGraph;
using System.Data.SqlServerCe;

namespace assignment
{
    public partial class Form1 : Form
    {


        SqlCeConnection myData = new SqlCeConnection(@"Data Source=F:\Uni Level 6\Software 2\assignment\MyDatabase#2.sdf");

        public Form1()
        {
            InitializeComponent();

            try
            {
                myData.Open();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
            
        }

        /// <summary>
        /// Strings to be used to set up the date time column. 
        /// </summary>
        string datetime;
        public string timer;
        public string ttime;
        public string tversion, tmonitor, tsmode, tmaxheart, tinterval, ttimedate;
        double interval;
        string resting;
        public double averspeed;
        public double spsum;
        public double rows;
        public string units;
        public TimeSpan totaltime;
        public double distance;
        public double[] Larray, Rarray, PIarray;
        public string result;
        long PowerB, leftMask, PedIndMask;
        string PBbinary, leftBin, PiBin;
        public double lvalue, pvalue, rvalue;
        double left, pedindex, right;

        private void Form1_Load(object sender, EventArgs e)
        {

        }



        public void mSpeed(double testSpeed, double testRows)
        {
            
            // equation used to help calulate average speed, so total of all speeds divided by rows. All of this is divided by 10 to calculate Kmph
            averspeed = (testSpeed / testRows) / 10;            
            // this statement is used if the miles button has been clicked.
            if (units == "miles")
            {
                averspeed = averspeed * 0.621371192;
            }

            //rounded to 2 decimal places
            averspeed = Math.Round(averspeed, 2);
            averageSpeed.Text = "Average Speed: " + averspeed.ToString();
        }




        public void mTotal(String ttimeB, double averspeedB)
        {
            //timespan used to calculate what value of 01:06:18.9 in hours and then multiplied by average speed to calculate the distance travelled
            totaltime = TimeSpan.Parse(ttimeB);
            distance = (averspeedB * double.Parse(totaltime.TotalHours.ToString()));
            distance = Math.Round(distance, 3);
            totalDistance.Text = "Total Distance: " + distance.ToString();
        }

        public void pedalBal(long value)
        {
            PBbinary = Convert.ToString(value, 2).PadLeft(16, '0');
            leftMask = PowerB & 255;
            leftBin = Convert.ToString(leftMask, 2).PadLeft(16, '0');
            lvalue = Convert.ToInt32(leftBin, 2);            
            rvalue = 100 - lvalue;            
            PedIndMask = value & 65280;
            PedIndMask = PedIndMask >> 8;
            PiBin = Convert.ToString(PedIndMask, 2).PadLeft(16, '0');
            pvalue = Convert.ToInt32(PiBin, 2);
            
        }


        public void run(String unit) 
        {
            //used so that data is only affected when converted to miles and km
            dataGridView1.Rows.Clear();
            dataGridView1.Refresh();

            //used to change strings which are for the date and the time into a Datetime  
            var timedate = DateTime.ParseExact(datetime + timer, "yyyyMMddHH:mm:ss.FFF", null);

            foreach (string item in listBox1.Items) //loop adds the datetime to each row along with a second to each row for each interval. 
            {
                if (item != "[HRData]")
                {

                    var tmp = timedate.ToString("dd/MM/yyyy HH:mm:ss.FFF") + "\t" + item;
                    var text = tmp.Split('\t', '\n');
                    dataGridView1.Rows.Add(text);
                    timedate = timedate.AddSeconds(1);
                    ttimedate = timedate.ToString();
                }

            }


            
            rows = dataGridView1.Rows.Count; //counts rows to help find averages
            spsum = 0; //used to calculate sum of all speeds
            for (int i = 0; i < dataGridView1.Rows.Count; ++i)
            {
                var tmpSpeed = dataGridView1.Rows[i].Cells["speed"].Value;

                //convert from string to an integer and then divide by 10 to get Kmph value
                spsum += Convert.ToInt32(tmpSpeed);

                double currentSpeed = double.Parse(tmpSpeed.ToString()) / 10;
                // then have if the miles button has been clicked then the value is put through this if statement.
                if (unit == "miles")
                {
                    currentSpeed = currentSpeed * 0.621371192;
                }
                // then places it in the table view in the Speed Column
                dataGridView1.Rows[i].Cells["speed"].Value = currentSpeed;
            }
            mSpeed(spsum, rows);
            //Max method used to search rows for the maximum value and again rounded to 2 decimal places
            double maxspeed = dataGridView1.Rows.Cast<DataGridViewRow>()
                    .Max(r => Convert.ToDouble(r.Cells["speed"].Value));

            maxspeed = Math.Round(maxspeed, 2);
            maxSpeed.Text = "Maximum Speed: " + maxspeed.ToString();


            //sum of power calculated and then converted to integer
            double psum = 0;
            for (int i = 0; i < dataGridView1.Rows.Count; ++i)
            {
                psum += Convert.ToInt32(dataGridView1.Rows[i].Cells["power"].Value);
            }
            //equation to solve average power
            double averpower = (psum / rows);
            averpower = Math.Round(averpower, 0);
            averagePower.Text = "Average Power: " + averpower.ToString();


            var maxpower = dataGridView1.Rows.Cast<DataGridViewRow>()
                    .Max(r => Convert.ToInt32(r.Cells["power"].Value));
            maxPower.Text = "Maximum Power: " + maxpower.ToString();

            //sum of total of all heart rates which is then converted to an integer
            double hrsum = 0;
            for (int i = 0; i < dataGridView1.Rows.Count; ++i)
            {
                hrsum += Convert.ToInt32(dataGridView1.Rows[i].Cells["heartrate"].Value);
            }
            //equation to solve average heart rate
            double averheartrate = (hrsum / rows);
            averheartrate = Math.Round(averheartrate, 0);
            averageHR.Text = "Average Heart rate: " + averheartrate.ToString() + " bpm";

            //maximum heartrate result found 
            double maxhrate = dataGridView1.Rows.Cast<DataGridViewRow>()
                    .Max(r => Convert.ToInt32(r.Cells["heartrate"].Value));
            mHeart(maxhrate);
            maxHR.Text = "Maximum Heart Rate: " + maxhrate.ToString() + " bpm";
            
            

            //minimum heartrate result found 
            var minhrate = dataGridView1.Rows.Cast<DataGridViewRow>()
                   .Min(r => Convert.ToInt32(r.Cells["heartrate"].Value));
            minHR.Text = "Minimum Heart Rate: " + minhrate.ToString() + " bpm";


            // used to calculate sum of total altitude to find average
            double asum = 0;
            for (int i = 0; i < dataGridView1.Rows.Count; ++i)
            {
                asum += Convert.ToInt32(dataGridView1.Rows[i].Cells["altitude"].Value);
            }
            // solution to find average altitude
            double averaltitude = (asum / rows);
            averaltitude = Math.Round(averaltitude, 0);
            averageAltitude.Text = "Average Altitude: " + averaltitude.ToString();

            //find maximum altitude
            var maxaltitude = dataGridView1.Rows.Cast<DataGridViewRow>()
                    .Max(r => Convert.ToInt32(r.Cells["altitude"].Value));
            maxAltitude.Text = "Maximum Altitude: " + maxaltitude.ToString();

            
            left = pedindex = right = 0;
            Larray = new double[dataGridView1.Rows.Count];
            Rarray = new double[dataGridView1.Rows.Count];
            PIarray = new double[dataGridView1.Rows.Count];
            for (int i = 0; i < dataGridView1.Rows.Count; ++i)
            {
                if (dataGridView1.Rows[i].Cells["powerBalance"].Value != null)
                {
                    
                    PowerB = Convert.ToInt64(dataGridView1.Rows[i].Cells["powerBalance"].Value);
                    pedalBal(PowerB);
                    Larray[i] = lvalue;
                    left += lvalue;
                    Rarray[i] = rvalue;
                    right += rvalue;
                    PIarray[i] = pvalue;
                    pedindex += pvalue;
                    
                }
            }

            left = Math.Round(left, 2);
            double averLeft = left / rows;
            averLeft = Math.Round(averLeft, 2);
            PBLeft.Text = " Average Power Balance Left: " + averLeft.ToString();

            right = Math.Round(right, 2);
            double averRight = right / rows;
            averRight = Math.Round(averRight, 2);
            PBRight.Text = " Average Power Balance Right: " + averRight.ToString();

            pedindex = Math.Round(pedindex, 2);
            double averpedInd = pedindex / rows;
            averpedInd = Math.Round(averpedInd, 2);
            PedInd.Text = " Pedalling Index " + averpedInd.ToString();
        
        }


        public void mHeart(double testHeart)
                {
	                string result;
	                if (testHeart > 195)
	                {
	                 result = "dead";
	                 //MessageBox.Show(result);
	                }
	                else if ( testHeart <= 195)
	                {
	                result = "alive";
                    //MessageBox.Show(result);	
	                }	
                }

        public void mPower(double averRight, double averLeft)
        {
             
            if (averRight + averLeft  != 100)
            {
                result = "Power Balance Wrong";
            }
            else if (averRight + averLeft == 100)
            {
                result = "Power Balance Values correct";
            }
        }

        private void header_Click(object sender, EventArgs e)
        {
            //clears listbox if any data already in it and then opens file dialog box to upload file
            listBox1.Items.Clear();
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            openFileDialog1.Title = "Open Text File";
            openFileDialog1.Filter = "All Files (*.*)|*.*";
            //openFileDialog1.InitialDirectory = @"C:\";
            string data;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                StreamReader reader = new StreamReader(openFileDialog1.FileName);

                using (reader)
                {
                    //looks for certain values from the file uploaded and replaces labels with these values(strings)
                    bool read = false;
                    while ((data = reader.ReadLine()) != null)
                    {
                        if (data.Contains("Version"))
                        {
                            var text = data.Split('=');
                            version.Text = (data);
                            tversion = text[1];
                        }

                        if (data.Contains("Monitor"))
                        {
                            var text = data.Split('=');
                            monitor.Text = (data);
                            tmonitor = text[1];
                        }


                        if (data.Contains("SMode"))
                        {
                            var text = data.Split('=');
                            smode.Text = (data);
                            tsmode = text[1];
                        }


                        if (data.Contains("Date"))
                        {
                            //splits date into date= and 20130205 to allow use for correct displaying in date time column 
                            var text = data.Split('=');
                            date.Text = (data);
                            datetime = text[1];

                        }

                        if (data.Contains("Length"))
                        {
                            //splits length into length= and 01:06:18.9 to make value usable to calculate total distance  
                            var text = data.Split('=');
                            duration.Text = (data);
                            ttime = text[1];
                        }


                        if (data.Contains("Interval"))
                        {
                            //splits interval into interval= and 1 to make value usable to calculate total distance  
                            var text = data.Split('=');
                            interval = Convert.ToDouble(text[1]);
                            tinterval = interval.ToString();
                            label1.Text = (data);
                        }


                        if (data.Contains("StartTime"))
                        {
                            //splits date into StartTime= and 15:46:20.0 to allow use for inserting and incrementing in time column
                            var text = data.Split('=');
                            startTime.Text = (data);
                            timer = text[1];
                        }


                        if (data.Contains("MaxHR"))
                        {
                            var text = data.Split('=');
                            maxHeart.Text = (data);
                            tmaxheart = text[1];
                            
                        }


                        if (data.Contains("RestHR"))
                        {
                            var text = data.Split('=');
                            restHR.Text = (data);
                            resting = text[1];
                        }


                        if (data.Contains("[HRData]"))
                        {
                            read = true;
                        }

                        if (read)
                        {
                            listBox1.Items.Add(data);
                        }
                    }
                }
                units = "km";
                run(units);
            }
            mTotal(ttime, averspeed);
        }


          public void insertheader(String date, String version, String monitor, String smode, String starttime, String duration, String maxhr, String resthr, String interval, String headerString)
        {
            try
            {

                SqlCeCommand cmdInsert = new SqlCeCommand(headerString, myData);

                cmdInsert.Parameters.AddWithValue("@date", date);
                cmdInsert.Parameters.AddWithValue("@version", version);
                cmdInsert.Parameters.AddWithValue("@monitor", monitor);
                cmdInsert.Parameters.AddWithValue("@smode", smode);
                cmdInsert.Parameters.AddWithValue("@starttime", starttime);
                cmdInsert.Parameters.AddWithValue("@duration", duration);
                cmdInsert.Parameters.AddWithValue("@maxhr", maxhr);
                cmdInsert.Parameters.AddWithValue("@resthr", resthr);
                cmdInsert.Parameters.AddWithValue("@interval", interval);
                cmdInsert.ExecuteNonQuery();
            }
            catch (SqlCeException ex)
            {
                MessageBox.Show("This data has already been saved");
                //MessageBox.Show(datetime + " .." + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }




        private void button2_Click(object sender, EventArgs e)
        {
            // Display as miles
            units = "miles";
            run(units);

        }

        private void button1_Click(object sender, EventArgs e)
        {
            // Display as KM
            units = "km";
            run(units);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Graph graph = new Graph();
            graph.gheart = new double[dataGridView1.Rows.Count];
            for (int i = 0; i < dataGridView1.Rows.Count; ++i)
            {
                graph.gheart[i] = Convert.ToInt32(this.dataGridView1.Rows[i].Cells["heartrate"].Value);
            }

            graph.gpower = new double[dataGridView1.Rows.Count];
            for (int i = 0; i < dataGridView1.Rows.Count; ++i)
            {
                graph.gpower[i] = Convert.ToInt32(this.dataGridView1.Rows[i].Cells["power"].Value);
            }

            graph.gspeed = new double[dataGridView1.Rows.Count];
            for (int i = 0; i < dataGridView1.Rows.Count; ++i)
            {
                graph.gspeed[i] = Convert.ToInt32(this.dataGridView1.Rows[i].Cells["speed"].Value);
            }

            graph.gcadence = new double[dataGridView1.Rows.Count];
            for (int i = 0; i < dataGridView1.Rows.Count; ++i)
            {
                graph.gcadence[i] = Convert.ToInt32(this.dataGridView1.Rows[i].Cells["cadence"].Value);
            }

            graph.galtitude = new double[dataGridView1.Rows.Count];
            for (int i = 0; i < dataGridView1.Rows.Count; ++i)
            {
                graph.galtitude[i] = Convert.ToInt32(this.dataGridView1.Rows[i].Cells["altitude"].Value);
            }

            graph.gtime = new DateTime[dataGridView1.Rows.Count];
            for (int i = 0; i < dataGridView1.Rows.Count; ++i)
            {
                graph.gtime[i] = Convert.ToDateTime(this.dataGridView1.Rows[i].Cells["time"].Value);
            }

            graph.powLeft = new double[dataGridView1.Rows.Count];
            graph.powRight = new double[dataGridView1.Rows.Count];
            graph.pIndex = new double[dataGridView1.Rows.Count];
            for (int i = 0; i < dataGridView1.Rows.Count; ++i)
            {
                graph.powLeft[i] = this.Larray[i];
                graph.powRight[i] = this.Rarray[i];
                graph.pIndex[i] = this.PIarray[i];
            }

            graph.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Calender newCalendar = new Calender();
            newCalendar.Show(this);
            
        }

        private void button5_Click(object sender, EventArgs e)
        {
            String checkdata = "SELECT COUNT (*) FROM HeaderData WHERE date ='" + datetime + "'";
            //MessageBox.Show(checkdata);
            SqlCeCommand check = new SqlCeCommand(checkdata, myData);
            int result = Convert.ToInt32(check.ExecuteScalar().ToString());
            if (result == 1)
            {
                MessageBox.Show("Data already exist");
            }


            else if (result == 0)
            {

                String headerString = "INSERT INTO HeaderData (date, version, monitor, smode, starttime, duration, maxhr, resthr, interval) VALUES (@date, @version, @monitor, @smode, @starttime, @duration, @maxhr, @resthr, @duration)";

                insertheader(datetime, tversion, tmonitor, tsmode, timer, ttime, tmaxheart, resting, tinterval, headerString);
                //insertheader(date.Text, version.Text, monitor.Text, smode.Text, startTime.Text, duration.Text, maxHeart.Text, restHR.Text, label1.Text, headerString);

                foreach (DataGridViewRow row in dataGridView1.Rows)
                {

                    SqlCeCommand cmdd = new SqlCeCommand("INSERT INTO RaceData (time, heartrate, speed, cadence, altitude, power, powerbalance, date) VALUES (@time, @heartrate, @speed, @cadence, @altitude, @power, @powerbalance, @date)", myData);

                    cmdd.Parameters.AddWithValue("@time", row.Cells["time"].Value);
                    cmdd.Parameters.AddWithValue("@heartrate", row.Cells["heartrate"].Value);
                    cmdd.Parameters.AddWithValue("@speed", row.Cells["speed"].Value);
                    cmdd.Parameters.AddWithValue("@cadence", row.Cells["cadence"].Value);
                    cmdd.Parameters.AddWithValue("@altitude", row.Cells["altitude"].Value);
                    cmdd.Parameters.AddWithValue("@power", row.Cells["power"].Value);
                    cmdd.Parameters.AddWithValue("@powerbalance", row.Cells["powerBalance"].Value);
                    cmdd.Parameters.AddWithValue("@date", datetime);
                    cmdd.ExecuteNonQuery();

                }
            }
        }

        public void updateListbox(string filedate)
        {
            String checkdata = "SELECT * FROM RaceData WHERE date ='" + filedate + "'";

            SqlCeCommand getData = new SqlCeCommand(checkdata, myData);

            SqlCeDataAdapter connection = new SqlCeDataAdapter(getData);

            DataTable racedata = new DataTable();
            connection.Fill(racedata);

            racedata.Columns.Remove("id");
            racedata.Columns.Remove("date");

            dataGridView1.DataSource = null;
            dataGridView1.Columns.Clear();
            dataGridView1.Refresh();

            dataGridView1.DataSource = racedata;
        }

        public void updatehead(string headdate)
        {
            String checkhead = "SELECT * FROM HeaderData WHERE date ='" + headdate + "'";
            SqlCeCommand getHData = new SqlCeCommand(checkhead, myData);

            //SqlCeDataReader mySqlDataReader = getHData.ExecuteReader();
            SqlCeDataAdapter headconnection = new SqlCeDataAdapter(getHData);
            DataTable headdata = new DataTable();
            headconnection.Fill(headdata);




            if (headdata.Columns.Contains("version"))
            {
                version.Text = ("Version:" + headdata);
            }

            if (headdata.Columns.Contains("monitor"))
            {
                monitor.Text = ("Monitor:" + headdata);
            }

            if (headdata.Columns.Contains("smode"))
            {
                smode.Text = ("S Mode:" + headdata);
            }

            if (headdata.Columns.Contains("date"))
            {
                date.Text = ("Date:" + headdata);
            }

            if (headdata.Columns.Contains("duration"))
            {
                duration.Text = ("Duration:" + headdata);
            }

            if (headdata.Columns.Contains("interval"))
            {
                label1.Text = ("Interval:" + headdata);
            }

            if (headdata.Columns.Contains("starttime"))
            {
                startTime.Text = ("Start Time:" + headdata);
            }

            if (headdata.Columns.Contains("maxhr"))
            {
                maxHR.Text = ("Max Heart Rate:" + headdata);
            }

            if (headdata.Columns.Contains("resthr"))
            {
                restHR.Text = ("Resting Heart Rate:" + headdata);
            }
        }
 


    }
}
