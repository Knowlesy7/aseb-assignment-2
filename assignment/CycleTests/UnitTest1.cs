﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using assignment;
using System.Windows.Forms;

namespace CycleTests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void heartcheck()
        {
            // Arrange
            double heartRate = 196;
            string expected = "dead";
            //Form1 test = new Form1();
            
            Form1 test = new Form1();
            // Act
            test.mHeart(heartRate);

            // Assert
            string actual = "dead";
            Assert.AreEqual(expected, actual, "You should be Dead");
        }
    }

    [TestClass]
    public class UnitTest2
    {
        [TestMethod]
        public void avercheck()
        {
            // Arrange
            double utSpeedSum = 100;
            double utRows = 5;
            double expected = 2;
            Form1 test = new Form1();

            // Act
            test.mSpeed(utSpeedSum, utRows);

            // Assert
            double actual = test.averspeed;
            Assert.AreEqual(expected, actual, 0.001, "Speed Not Converted Correctly");
        }
    }

    [TestClass]
    public class UnitTest3
    {
        [TestMethod]
        public void distcheck()
        {
            // Arrange
            String utTime = "00:45:00";
            double utAverSpeed = 30;

            double expected = 22.5;
            Form1 test = new Form1();

            // Act
            test.mTotal(utTime, utAverSpeed);

            // Assert
            double actual = test.distance;
            Assert.AreEqual(expected, actual, 0.001, "Distance Not Converted Correctly");
        }
    }

    [TestClass]
    public class UnitTest4
    {
        [TestMethod]
        public void powercheck()
        {
            // Arrange
            double leftpower = 58.5;
            double rightpower = 41.5;

            string expected = "Power Balance Values correct";
            Form1 test = new Form1();

            // Act
            test.mPower(leftpower, rightpower);

            // Assert
            string actual = test.result;
            Assert.AreEqual(expected, actual, "Power Balance is correct");
        }
    }

    [TestClass]
    public class UnitTest5
    {
        [TestMethod]
        public void Pedcheck()
        {
            // Arrange
            long pedvalue = 1111;

            double expected = 4;
            Form1 test = new Form1();

            // Act
            test.pedalBal(pedvalue);

            // Assert
            double actual = test.pvalue;
            Assert.AreEqual(expected, actual, 0.001, "Pedal Index is correct");
        }
    }





}

